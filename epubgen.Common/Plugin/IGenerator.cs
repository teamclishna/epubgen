﻿using System;
using System.Collections.Generic;
using System.Text;

using epubgen.Types;

using clUtils.log;
using clUtils.util;

namespace epubgen.Plugin
{
    /// <summary>
    /// epub作成用プラグインのインターフェイス
    /// </summary>
    public interface IGenerator
    {
        /// <summary>
        /// type パラメータで指定するプラグイン名.
        /// </summary>
        string PluginType { get; }

        /// <summary>
        /// プラグインのバージョン情報.
        /// 実体としてはプラグインのアセンブリのバージョン情報を返す
        /// </summary>
        Version VersionInfo { get; }

        /// <summary>
        /// epub ファイルの生成
        /// </summary>
        /// <param name="cmmParams">epub生成パラメータの共通部分</param>
        /// <param name="options">コマンドラインパラメータ</param>
        /// <param name="logger">進行状況を出力するロガー</param>
        /// <returns>生成結果</returns>
        GenerateResult Generate(CommonParams cmmParams, CommandLineOptions options, ILogger logger);

        /// <summary>
        /// usageの表示.
        /// プラグイン固有のusageを表示する場合にその文字列を返す.
        /// </summary>
        /// <returns>usage文字列. usageが必要ない場合は空文字列を返す.</returns>
        string Usage();
    }
}
