﻿using System;
using System.Collections.Generic;
using System.Text;

namespace epubgen.Constants
{
    /// <summary>
    /// コマンドライン引数で指定する共通部分のパラメータ名
    /// </summary>
    public static class CommonArgNames
    {
        /// <summary>epub生成処理のプラグイン名</summary>
        public static readonly string TYPE_LONG = "type";
        /// <summary>実行パラメータをJSONで指定する場合のファイルパス</summary>
        public static readonly string INPUT_FILE_LONG = "input-file";
        /// <summary>書籍のタイトル</summary>
        public static readonly string TITLE_LONG = "title";
        /// <summary>書籍の作者</summary>
        public static readonly string AUTHOR_LONG = "author";
        /// <summary>epubファイルのUUID</summary>
        public static readonly string UUID_LONG = "uuid";
        /// <summary>出力するepubファイルのパス</summary>
        public static readonly string OUTPUT_PATH_LONG = "output";
        /// <summary>ページの綴じ方向</summary>
        public static readonly string PAGE_DIRECTION_LONG = "page-direction";
        /// <summary>epubファイルを生成しない</summary>
        public static readonly string NO_CREATE_EPUB_LONG = "no-create-epub";
        /// <summary>作業ディレクトリを削除しない</summary>
        public static readonly string NO_DELETE_WORKDIR_LONG = "no-delete-workdir";
        /// <summary>epubファイルの上書きを許可する</summary>
        public static readonly string ALLOW_OVERWRITE_LONG = "allow-overwrite";
        /// <summary>出力するepubファイルのパス</summary>
        public static readonly string TEMP_DIR_LONG = "temp-dir";
        /// <summary>冗長なメッセージを出力する</summary>
        public static readonly string VERBOSE_LONG = "verbose";
        /// <summary>usageを表示する</summary>
        public static readonly string USAGE_LONG = "help";
    }
}
