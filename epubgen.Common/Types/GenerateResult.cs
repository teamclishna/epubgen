﻿using System;
using System.Collections.Generic;
using System.Text;

namespace epubgen.Types
{
    /// <summary>
    /// 生成結果を示す列挙子
    /// </summary>
    public enum GenerateResultEnum
    {
        /// <summary>正常終了</summary>
        SUCCESS,
        /// <summary>既存のopfファイルを使用する場合、その書式が不正</summary>
        INVALID_IDENTIFIER,
        /// <summary>生成しようとした epub ファイルが既に存在する</summary>
        EPUB_ALREADY_EXISTS,
        /// <summary>既存の epub ファイルを削除することに失敗した</summary>
        EPUB_DELETE_FAILED,
        /// <summary>異常終了</summary>
        FAILED,
    }

    /// <summary>
    /// epub ファイル生成結果
    /// </summary>
    public class GenerateResult
    {
        /// <summary>
        /// 処理結果.
        /// 処理が正常終了した場合は SUCCESS
        /// </summary>
        public GenerateResultEnum Result { get; set; }

        /// <summary>
        /// 出力した epub ファイルのパス.
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// 処理中に例外スローされた場合、その内容
        /// </summary>
        public Exception Exception { get; set; }
    }
}
