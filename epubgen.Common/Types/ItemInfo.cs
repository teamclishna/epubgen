﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace epubgen.Types
{
    /// <summary>
    /// epub 内に格納するアイテム1個の情報を示す構造体
    /// </summary>
    public class ItemInfo
    {
        /// <summary>
        /// このアイテムの情報
        /// </summary>
        public String ID { get; set; }

        /// <summary>
        /// このアイテムの MIME 情報
        /// </summary>
        public String MediaType { get; set; }

        /// <summary>
        /// このアイテムの実体を示すファイル
        /// </summary>
        public FileInfo ItemFile { get; set; }

        /// <summary>
        /// このアイテムを配置する場所の相対パス
        /// </summary>
        public String MediaPath { get; set; }
    }

    /// <summary>
    /// 見開き方向の情報
    /// </summary>
    public enum PageSpread
    {
        /// <summary>
        /// 指定なし
        /// </summary>
        None,
        /// <summary>
        /// 左ページから開始
        /// </summary>
        SpreadLeft,
        /// <summary>
        /// 右ページから開始
        /// </summary>
        SPreadRight,
    }

    /// <summary>
    /// epubのページとして参照可能なアイテムの情報
    /// </summary>
    public class ItemRefInfo
    {
        /// <summary>
        /// 参照ID. ItemInfo に設定したIDを指定する
        /// </summary>
        public String RefID { get; set; }

        /// <summary>
        /// ページが連続するかどうか
        /// </summary>
        public bool IsLinear { get; set; }

        /// <summary>
        /// 見開きのページ方向
        /// </summary>
        public PageSpread PageSpreadMode { get; set; }
    }
}
