﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace epubgen.Types
{
    /// <summary>
    /// コマンドラインパラメータ、またはjsonファイルから取得する処理内容パラメータ
    /// </summary>
    public class CommonParams
    {
        /// <summary>
        /// 生成される電子書籍を識別するGUID
        /// </summary>
        public Guid epubID { get; set; }

        /// <summary>
        /// 生成するepubファイルのパス.
        /// パスが指定されていない場合はカレントディレクトリにGUIDをファイル名として生成する.
        /// </summary>
        public string epubFilePath { get; set; }

        /// <summary>
        /// 作業用ディレクトリを作成するディレクトリ.
        /// </summary>
        public DirectoryInfo WorkingBaseDirectory { get; set; }

        /// <summary>
        /// 作業用ディレクトリの情報.
        /// WorkingBaseDirectory 直下に epub ファイルの ID をディレクトリ名として作成する.
        /// </summary>
        public DirectoryInfo WorkingDirectory { get; set; }

        /// <summary>
        /// 生成しようとする epub ファイルが既に存在する場合、上書きを許可するなら true
        /// </summary>
        public bool AllowOverwrite { get; set; }

        /// <summary>
        /// この書籍のタイトル
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// この書籍の作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// この書籍の表紙画像.
        /// 各書籍形式の生成処理中で、epubの作業ディレクトリ内に配置した表紙画像の情報を設定する.
        /// </summary>
        public FileInfo CoverImageFile { get; set; }

        /// <summary>
        /// 書籍のページ綴じ方向
        /// </summary>
        public PageDirection PageDirection { get; set; }

        /// <summary>
        /// 生成する電子書籍の入力データの種類.
        /// ここで指定した名前に一致するプラグインを使って実行する.
        /// </summary>
        public string BookType { get; set; }

        /// <summary>
        /// epubファイルの元になるファイル群まで生成し、epubファイル自体は生成しない.
        /// </summary>
        public bool DontGenerateEpub { get; set; }

        /// <summary>
        /// epubファイル生成後に作業用ディレクトリを削除しない.
        /// DontUseEpub プロパティが true の場合、このプロパティは無視する.
        /// </summary>
        public bool DontDeleteWorkDir { get; set; }

        /// <summary>
        /// 詳細な進行状況を出力する
        /// </summary>
        public bool Verbose { get; set; }

        /// <summary>
        /// コマンドラインヘルプを表示する.
        /// これが true の場合、他の全てのパラメータを無視する.
        /// </summary>
        public bool ShowUsage { get; set; }
    }

    /// <summary>
    /// 書籍のページ方向
    /// </summary>
    public enum PageDirection
    {
        /// <summary>左綴じ (左から右、横書き用)</summary>
        LeftToRight,
        /// <summary>右綴じ (右から左、縦書き用)</summary>
        RightToLeft,
    }
}
