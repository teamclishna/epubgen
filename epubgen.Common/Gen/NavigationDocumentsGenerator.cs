﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Text;

using clUtils.log;
using epubgen.Types;

namespace epubgen.Gen
{
    /// <summary>
    /// navigation-documents.xhtml (目次情報のファイル) を生成するためのユーティリティクラス
    /// </summary>
    public class NavigationDocumentsGenerator
    {
        private static readonly string TEMPLATE_REPLACE_TOC_TITLE = "%TOC_TITLE%";
        private static readonly string TEMPLATE_REPLACE_TOC_LIST = "%TOC_LIST%";

        /// <summary>
        /// 目次ページとなるxhtmlファイルを生成する
        /// </summary>
        /// <param name="baseDir">ファイルを生成するディレクトリ</param>
        /// <param name="title">目次の先頭に表示するタイトル</param>
        /// <param name="templateXhtml">テンプレートのxhtml文字列</param>
        /// <param name="tocItemList">目次にリンクを生成するアイテム情報</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>生成した目次ファイルのアイテム情報. このアイテムもopfのエントリに追加しなければならない</returns>
        public static ItemInfo CreateNavigationDocumentsFile(
            DirectoryInfo baseDir,
            string title,
            string templateXhtml,
            IEnumerable<ItemInfo> tocItemList,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate Table of Contents...");

            try
            {
                // テンプレートを読み込み、置換部分を作品タイトルに入れ替える
                var fileData = templateXhtml;
                fileData = fileData.Replace(TEMPLATE_REPLACE_TOC_TITLE, title);

                // 目次のリストを生成する
                var sbTOC = new StringBuilder();
                foreach (var item in tocItemList)
                { sbTOC.Append(string.Format("<li><a href=\"{0}\">{1}</a></li>", item.MediaPath, title)); }

                fileData = fileData.Replace(TEMPLATE_REPLACE_TOC_LIST, sbTOC.ToString());

                var writeFilePath = Path.Combine(baseDir.FullName, "navigation-documents.xhtml");
                var xmlFileInfo = new FileInfo(writeFilePath);
                logger.PrintPush(LogLevel.INFO, string.Format("Generate Table of Contents file {0}", xmlFileInfo.FullName));

                using (var sw = new StreamWriter(writeFilePath))
                { sw.Write(fileData); }

                logger.PrintPop();
                return new ItemInfo()
                {
                    ID = "toc",
                    MediaPath = "navigation-documents.xhtml",
                    ItemFile = xmlFileInfo,
                    MediaType = MimeTypeUtils.GetMime(xmlFileInfo),
                };
            }
            finally
            { logger.PrintPop(); }
        }
    }
}
