﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using clUtils.util;

using epubgen.Constants;
using epubgen.Types;

namespace epubgen.Gen
{
    /// <summary>
    /// コマンドライン引数から共通パラメータを取得するためのユーティリティクラス
    /// </summary>
    public static class ArgsParseUtils
    {
        /// <summary>
        /// コマンドラインパラメータ
        /// (共通部分のみ. 生成方法毎のパラメータは各DLL内で再度パースして取得する)
        /// </summary>
        public static CommandLineOptionDefinition[] COMMON_ARGS_TEMPLATE =
        {
            // 入力とするリソースのタイプ
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.TYPE_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },
            // 実行パラメータを指定した json ファイルのパス
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.INPUT_FILE_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },
            // epub ファイルに設定する UUID
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.UUID_LONG, ShortSwitch = 'u', RequireParam = true, AllowMultiple = false },
            // 出力する epub ファイルのフルパス
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.OUTPUT_PATH_LONG, ShortSwitch = 'o', RequireParam = true, AllowMultiple = false },
            
            // 書籍の綴じ方向
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.PAGE_DIRECTION_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },

            // epubファイルを生成しない
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.NO_CREATE_EPUB_LONG, ShortSwitch = '\0', RequireParam = false, AllowMultiple = false },
            // 作業用フォルダを削除しない
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.NO_DELETE_WORKDIR_LONG, ShortSwitch = '\0', RequireParam = false, AllowMultiple = false },
            // epub ファイルの上書きを許可する
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.ALLOW_OVERWRITE_LONG, ShortSwitch = '\0', RequireParam = false, AllowMultiple = false },

            // 作業用フォルダの生成元フォルダ. 省略時はカレントフォルダ
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.TEMP_DIR_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },
            // 書籍のタイトル
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.TITLE_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },
            // 書籍の作者名
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.AUTHOR_LONG, ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },

            // 詳細なメッセージを表示する
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.VERBOSE_LONG, ShortSwitch = 'v', RequireParam = false, AllowMultiple = false },
            // コマンドラインヘルプを表示する
            new CommandLineOptionDefinition(){ LongSwitch = CommonArgNames.USAGE_LONG, ShortSwitch = 'h', RequireParam = false, AllowMultiple = false },
        };

        /// <summary>
        /// 共通パラメータの初期値を生成して返す
        /// </summary>
        /// <returns>生成した初期値</returns>
        public static CommonParams InitParameters()
        {
            return new CommonParams()
            {
                BookType = null,
                Author = null,
                Title = null,
                epubID = Guid.Empty,
                epubFilePath = null,
                WorkingBaseDirectory = new DirectoryInfo(Environment.CurrentDirectory),
                PageDirection = PageDirection.LeftToRight,
                DontDeleteWorkDir = false,
                DontGenerateEpub = false,
                AllowOverwrite = false,
                Verbose = false,
                ShowUsage = false,
            };
        }

        /// <summary>
        /// json ファイルからパラメータを読み込み、引数で渡したパラメータに上書きして返す.
        /// </summary>
        /// <param name="commParams">共通パラメータの初期値</param>
        /// <param name="options">コマンドライン引数から取得したパラメータ群</param>
        /// <returns>
        /// --input-file パラメータで指定したJSONファイルから取得したパラメータを commParams に上書きした値.
        /// パラメータが指定されていない場合は何もしない
        /// </returns>
        public static CommonParams SetParametersFromJson(this CommonParams commParams, CommandLineOptions options)
        {
            if(!options.Options.ContainsKey("input-file"))
            { return commParams; }

            throw new NotImplementedException("jsonファイルの読み込みは未実装です.");
        }

        /// <summary>
        /// コマンドライン引数から取得したパラメータを引数で渡したパラメータに上書きして返す.
        /// </summary>
        /// <param name="commParams">共通パラメータの初期値</param>
        /// <param name="options">コマンドライン引数から取得したパラメータ群</param>
        /// <returns>options から取得した共通パラメータを commParams に上書きした値.</returns>
        public static CommonParams SetParaemtersFromArgs(
            this CommonParams commParams,
            CommandLineOptions options)
        {
            return new CommonParams()
            {
                BookType = (options.Options.ContainsKey("type") ? options.Options["type"].ElementAt(0) : commParams.BookType),
                Author = (options.Options.ContainsKey("author") ? options.Options["author"].ElementAt(0) : commParams.Author),
                Title = (options.Options.ContainsKey("title") ? options.Options["title"].ElementAt(0) : commParams.Title),
                epubID = (options.Options.ContainsKey("uuid") ? Guid.Parse(options.Options["uuid"].ElementAt(0)) : commParams.epubID),
                epubFilePath = (options.Options.ContainsKey("output") ? options.Options["output"].ElementAt(0) : commParams.epubFilePath),
                WorkingBaseDirectory = (options.Options.ContainsKey("temp-dir") ? new DirectoryInfo(options.Options["temp-dir"].ElementAt(0)) : commParams.WorkingBaseDirectory),
                PageDirection = (options.Options.ContainsKey("page-direction") ? EpubGeneratorUtil.ParsePageDirection(options.Options["page-direction"].ElementAt(0)) : commParams.PageDirection),
                DontDeleteWorkDir = (options.Options.ContainsKey("no-delete-workdir") ? true : commParams.DontDeleteWorkDir),
                DontGenerateEpub = (options.Options.ContainsKey("no-create-epub") ? true : commParams.DontGenerateEpub),
                AllowOverwrite = (options.Options.ContainsKey("allow-overwrite") ? true : commParams.AllowOverwrite),
                Verbose = (options.Options.ContainsKey("verbose") ? true : commParams.Verbose),
                ShowUsage = (options.Options.ContainsKey("help") ? true : commParams.ShowUsage),
            };
        }

    }
}
