﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Text;

using clUtils.log;
using epubgen.Types;

namespace epubgen.Gen
{
    /// <summary>
    /// OPFファイルの場所を示す container.xml ファイルを生成するユーティリティクラス
    /// </summary>
    public static class ContainerGenerator
    {
        private static readonly string DIRNAME_METAINF = "META-INF";

        private static readonly string TEMPLATE_REPLACE_OPF_NAME = "%OPF_FILE_NAME%";

        /// <summary>
        /// container.xml を生成する
        /// </summary>
        /// <param name="baseDir"></param>
        /// <param name="templateXMLstring"></param>
        /// <param name="opfFileName"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static FileInfo CreateContainerFile(
            DirectoryInfo baseDir,
            string templateXMLstring,
            string opfFileName,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate container.xml file...");

            // META-INF ディレクトリを作成する
            var metainfDir = baseDir.GetDirectories(DIRNAME_METAINF)[0];

            var xmlFileString = templateXMLstring.Replace(TEMPLATE_REPLACE_OPF_NAME, opfFileName);
            var writeFilePath = Path.Combine(metainfDir.FullName, "container.xml");
            var xmlFileInfo = new FileInfo(writeFilePath);

            logger.PrintPush(LogLevel.INFO, string.Format("Generate container.xml file {0}", xmlFileInfo.FullName));

            using (var sw = new StreamWriter(writeFilePath))
            { sw.Write(xmlFileString); }

            logger.PrintPop();
            logger.PrintPop();
            return new FileInfo(writeFilePath);
        }
    }
}
