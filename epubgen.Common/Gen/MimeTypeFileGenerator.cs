﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using clUtils.log;

namespace epubgen.Gen
{
    /// <summary>
    /// MIME-TYPE 定義ファイルの出力ユーティリティクラス
    /// </summary>
    public static class MimeTypeFileGenerator
    {
        /// <summary>
        /// mimetype ファイルを生成する
        /// </summary>
        /// <param name="baseDir">生成するディレクトリ</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>生成したファイルの情報</returns>
        public static FileInfo CreateMimeTypeFile(
            DirectoryInfo baseDir,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate mime type entry...");

            var filePath = Path.Combine(baseDir.FullName, "mimetype");
            using (var st = new StreamWriter(filePath))
            { st.Write("application/epub+zip"); }

            logger.PrintPop();
            return new FileInfo(filePath);
        }
    }
}
