﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using clUtils.log;
using clUtils.xml;

using epubgen.Types;

namespace epubgen.Gen
{
    /// <summary>
    /// epub 生成のためのユーティリティクラス
    /// </summary>
    public static class EpubGeneratorUtil
    {
        /// <summary>
        /// epub ファイルの出力先のフルパスを生成する.
        /// --output スイッチでファイル名を指定している場合はそのファイルパス.
        /// 指定していない場合は epub ファイルの GUID を元に設定する.
        /// </summary>
        /// <param name="param">共通部分のパラメータ</param>
        /// <returns>生成したファイルのフルパス</returns>
        public static string CreateEpubFilePath(CommonParams param)
        {
            // 出力先パスが明示的に設定されていればそれを返す
            if (param.epubFilePath != null)
            {
                var fi = new FileInfo(param.epubFilePath);
                if (!fi.Directory.Exists)
                { throw new DirectoryNotFoundException(string.Format("Epub output directory \"{0}\" not found.", fi.Directory.FullName)); }
                else
                { return fi.FullName; }
            }

            // 出力先が指定されていない場合、epubファイルのパスを元にカレントディレクトリに出力する
            if (!Guid.Empty.Equals(param.epubID))
            {
                var fi = new FileInfo(
                    Path.Combine(
                        System.Environment.CurrentDirectory,
                        param.epubID.ToString("D").Replace("-", "") + ".epub"));
                return fi.FullName;
            }

            // パラメータの設定が不十分
            throw new InvalidOperationException("Cannot specify epub file name. require --output switch.");
        }

        /// <summary>
        /// 指定したディレクトリの内容を圧縮して epub ファイルを生成する
        /// </summary>
        /// <param name="inputDir">epub のリソースを格納したディレクトリ</param>
        /// <param name="outputEpubName">生成する epub ファイルのフルパス</param>
        /// <param name="allowOverwrite">生成しようとする epub ファイルが存在した場合に新しい出力で置き換えて良い場合は true</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>ファイルの生成結果</returns>
        public static GenerateResult GenerateEpub(
            DirectoryInfo inputDir,
            string outputEpubName,
            bool allowOverwrite,
            ILogger logger)
        {
            logger.PrintPush(
                LogLevel.INFO,
                string.Format(
                    "Generate epub file {0} -> {1}.",
                    inputDir.FullName,
                    outputEpubName));
            try
            {
                var prevFile = new FileInfo(outputEpubName);
                if (prevFile.Exists)
                {
                    if (allowOverwrite)
                    {
                        try
                        { prevFile.Delete(); }
                        catch (Exception eUnknown)
                        {
                            logger.Print(LogLevel.ERROR, string.Format("ERROR: exists epub file \"{0}\" delete failed.", outputEpubName));
                            return new GenerateResult()
                            {
                                Result = GenerateResultEnum.EPUB_DELETE_FAILED,
                                Exception = eUnknown,
                                OutputPath = null,
                            };
                        }
                    }
                    else
                    {
                        logger.Print(LogLevel.ERROR, string.Format("ERROR: epub file \"{0}\" already exists.", outputEpubName));
                        return new GenerateResult()
                        {
                            Result = GenerateResultEnum.EPUB_ALREADY_EXISTS,
                            Exception = null,
                            OutputPath = null,
                        };
                    }
                }

                using (var z = ZipFile.Open(outputEpubName, ZipArchiveMode.Create))
                {
                    // 作業用ディレクトリのルートにあるファイルを追加.
                    // mimetypeファイルを最初に追加する必要がある
                    AddEntry(z, Path.Combine(inputDir.FullName, "mimetype"), "mimetype", logger);

                    // 残りのファイルを追加
                    foreach (var f in inputDir.GetFiles().Where((x) => { return x.Name != "mimetype"; }))
                    { AddEntry(z, f.FullName, f.Name, logger); }

                    // サブディレクトリのファイルを再帰的に格納する
                    foreach (var d in inputDir.GetDirectories())
                    { AddEntrySubFile(z, d, d.Name + "/", logger); }

                    return new GenerateResult()
                    {
                        Result = GenerateResultEnum.SUCCESS,
                        Exception = null,
                        OutputPath = outputEpubName,
                    };
                }
            }
            catch (Exception eUnknown)
            {
                return new GenerateResult()
                {
                    Result = GenerateResultEnum.FAILED,
                    Exception = eUnknown,
                    OutputPath = null,
                };
            }
            finally
            {
                logger.PrintPop();
            }
        }

        /// <summary>
        /// zipファイルにエントリを追加する
        /// </summary>
        /// <param name="z">エントリの追加対象の zip エントリ</param>
        /// <param name="addFile">追加するファイルのフルパス</param>
        /// <param name="entryPath">追加するファイルの zip ファイル内のパス</param>
        /// <param name="logger">進行状況を出力するロガー</param>
        private static void AddEntry(
            ZipArchive z,
            string addFile,
            string entryPath,
            ILogger logger)
        {
            logger.Print(LogLevel.VERBOSE, string.Format("Add Entry: {0} -> {1}", addFile, entryPath));
            z.CreateEntryFromFile(addFile, entryPath);
        }

        /// <summary>
        /// サブディレクトリのファイルを再帰的に圧縮する
        /// </summary>
        /// <param name="z">エントリの追加対象の zip エントリ</param>
        /// <param name="d">エントリの追加対象のファイルが格納されているディレクトリ</param>
        /// <param name="directoryPath">エントリ追加時の zip エントリ内のディレクトリパス</param>
        /// <param name="logger">進行状況を出力するロガー</param>
        private static void AddEntrySubFile(
            ZipArchive z,
            DirectoryInfo d,
            string directoryPath,
            ILogger logger)
        {
            // このディレクトリ内のファイルをエントリに追加する
            foreach (var f in d.GetFiles())
            { AddEntry(z, f.FullName, directoryPath + f.Name, logger); }

            // このディレクトリのサブディレクトリを再帰的に格納する
            foreach (var subdir in d.GetDirectories())
            { AddEntrySubFile(z, subdir, directoryPath + subdir.Name + "/", logger); }
        }

        /// <summary>
        /// 指定したディレクトリにある .opf ファイルから epub の ID を取得してGUIDとして返す
        /// </summary>
        /// <param name="inputDir"></param>
        /// <returns></returns>
        public static Guid GetEpubIDFromProject(DirectoryInfo inputDir, ILogger logger)
        {
            logger.PrintPush(LogLevel.VERBOSE, string.Format("unique-id search from \"{0}\"", inputDir.FullName));
            try
            {
                // ディレクトリの *.opf ファイルを探す
                var opfFiles = inputDir.GetFiles("*.opf");
                if (opfFiles.Length != 1)
                {
                    logger.Print(LogLevel.ERROR, "ERROR: *.opf file must only one file.");
                    return Guid.Empty;
                }
                var reader = new XmlReader() { Logger = logger, };
                var xmlFile = reader.LoadFromFile(opfFiles[0].FullName);

                // /metadata/dc:identifier を取得する
                try
                {
                    var node = xmlFile.FindNode(new string[] { "metadata", "identifier" }).Text;
                    logger.Print(LogLevel.VERBOSE, string.Format("identifier found: {0}", node));

                    Guid result;
                    if (Guid.TryParse(node.Replace("urn:uuid:", "").Replace("-", ""), out result))
                    { return result; }
                    else
                    {
                        logger.Print(LogLevel.ERROR, string.Format("ERROR: invalid identifier \"{0}\"", node));
                        return Guid.Empty;
                    }
                }
                catch(Exception)
                {
                    // ノードの事態の取得に失敗
                    logger.Print(LogLevel.ERROR, "ERROR: invalid opf file. dc:identifier node not found.");
                    return Guid.Empty;
                }
            }
            finally
            { logger.PrintPop(); }
        }

        /// <summary>
        /// 指定した文字列からページの綴じ方向を示す
        /// </summary>
        /// <param name="pd"></param>
        /// <returns></returns>
        public static PageDirection ParsePageDirection(string pd)
        {
            switch (pd.Trim().ToLower())
            {
                case "left":
                case "ltr":
                    return PageDirection.LeftToRight;

                case "right":
                case "rtl":
                    return PageDirection.RightToLeft;

                default:
                    throw new ArgumentException(string.Format("invalid page direction parameter \"{0}\"", pd));
            }
        }

        /// <summary>
        /// 指定したGUIDを元にファイル名に使用する文字列を生成する.
        /// </summary>
        /// <param name="id">GUID</param>
        /// <returns>id を元に生成したファイル名. GUIDの文字列表現からハイフンを取り除いた文字列を返す.</returns>
        public static string Guid2String(this Guid id)
        { return id.ToString("D").Replace("-", ""); }
    }
}
