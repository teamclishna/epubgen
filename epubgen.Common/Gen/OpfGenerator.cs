﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

using System.Text;

using clUtils.log;
using epubgen.Types;

namespace epubgen.Gen
{
    /// <summary>
    /// Opfファイルを生成するためのユーティリティクラス
    /// </summary>
    public static class OpfGenerator
    {
        /// <summary>
        /// OPFファイルを生成する
        /// </summary>
        /// <param name="dir">ファイルを生成するディレクトリ</param>
        /// <param name="FileName">生成するファイル名</param>
        /// <param name="cmmParams">共通パラメータ</param>
        /// <param name="itemInfoList">epubファイルに登録するアイテム情報</param>
        /// <param name="itemRefInfoList">epubのページに表示するアイテムの参照情報</param>
        /// <param name="logger">ロガーオブジェクト</param>
        public static FileInfo CreateOpfFile(
            DirectoryInfo dir,
            string FileName,
            CommonParams cmmParams,
            IEnumerable<ItemInfo> itemInfoList,
            IEnumerable<ItemRefInfo> itemRefInfoList,
            ILogger logger)
        { return CreateOpfFile(Path.Combine(dir.FullName, FileName), cmmParams, itemInfoList, itemRefInfoList, logger); }

        /// <summary>
        /// OPFファイルを生成する
        /// </summary>
        /// <param name="opfFileFullpath">生成するファイルのフルパス</param>
        /// <param name="cmmParams">共通パラメータ</param>
        /// <param name="itemInfoList">epubファイルに登録するアイテム情報</param>
        /// <param name="itemRefInfoList">epubのページに表示するアイテムの参照情報</param>
        /// <param name="logger">ロガーオブジェクト</param>
        public static FileInfo CreateOpfFile(
            string opfFileFullpath,
            CommonParams cmmParams,
            IEnumerable<ItemInfo> itemInfoList,
            IEnumerable<ItemRefInfo> itemRefInfoList,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate Opf data...");

            var doc = new XDocument();

            XNamespace xmlNameSpace = "http://www.idpf.org/2007/opf";
            var packageNode = new XElement(xmlNameSpace + "package");
            packageNode.Add(new XAttribute("version", "3.0"));
            packageNode.Add(new XAttribute(xmlNameSpace + "lang", "ja"));
            packageNode.Add(new XAttribute("unique-identifier", "unique-id"));
            packageNode.Add(new XAttribute("prefix", "ebpaj: http://www.ebpaj.jp/"));

            packageNode.Add(CreateMetadataNode(cmmParams, xmlNameSpace, logger));
            packageNode.Add(CreateManifestNode(cmmParams, itemInfoList, logger));
            packageNode.Add(CreateSpineNode(cmmParams, itemRefInfoList, logger));

            doc.Add(packageNode);

            // xml ファイルに出力する
            logger.PrintPush(LogLevel.INFO, string.Format("Write opf file: {0}", opfFileFullpath));
            using (var s = new FileStream(opfFileFullpath, FileMode.CreateNew))
            { doc.Save(s); }
            logger.PrintPop();

            logger.PrintPop();
            return new FileInfo(opfFileFullpath);
        }

        /// <summary>
        /// metadata ノードを生成する
        /// </summary>
        /// <param name="param">電子書籍のパラメータ</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>metadata ノード配下の xml 文字列</returns>
        private static XElement CreateMetadataNode(
            CommonParams param,
            XNamespace xmlnsNamespace,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate metadata node.");

            var metadataNode = new XElement("metadata");
            XNamespace dcNameSpace = "http://purl.org/dc/elements/1.1/";
            metadataNode.Add(new XAttribute(XNamespace.Xmlns + "dc", dcNameSpace));

            var titleNode = new XElement(dcNameSpace + "title");
            titleNode.Add(new XAttribute("id", "title"));
            titleNode.Add(new XText(string.IsNullOrWhiteSpace(param.Title) ? "NO TITLE" : param.Title));
            metadataNode.Add(titleNode);

            var creatorNode = new XElement(dcNameSpace + "creator");
            creatorNode.Add(new XAttribute("id", "creator01"));
            creatorNode.Add(new XText(string.IsNullOrWhiteSpace(param.Author) ? "NO AUTHOR" : param.Author));
            metadataNode.Add(creatorNode);

            var langNode = new XElement(dcNameSpace + "language");
            langNode.Add(new XText("ja"));
            metadataNode.Add(langNode);

            var uuidNode = new XElement(dcNameSpace + "identifier");
            uuidNode.Add(new XText(string.Format("urn:uuid:{0}", param.epubID)));
            metadataNode.Add(uuidNode);

            var modifiedNode = new XElement("meta");
            modifiedNode.Add(new XAttribute("property", "dcterms:modified"));
            modifiedNode.Add(new XText(string.Format("{0:yyyy-MM-ddTHH:mm:ssZ}", DateTime.Now)));
            metadataNode.Add(modifiedNode);

            var guideVersionNode = new XElement("meta");
            guideVersionNode.Add(new XAttribute("property", "ebpaj:guide-version"));
            guideVersionNode.Add(new XText("1.1.3"));
            metadataNode.Add(guideVersionNode);

            logger.PrintPop();
            return metadataNode;
        }

        /// <summary>
        /// manifest ノードを生成する
        /// </summary>
        /// <param name="param">電子書籍のパラメータ</param>
        /// <param name="items">電子書籍中に格納するファイルのアイテム情報</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>manifest ノード配下の xml 文字列</returns>
        private static XElement CreateManifestNode(
            CommonParams param,
            IEnumerable<ItemInfo> items,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate manifest node.");

            var manifestNode = new XElement("manifest");

            foreach (var i in items)
            {
                var itemNode = new XElement("item");
                itemNode.Add(new XAttribute("id", i.ID));
                itemNode.Add(new XAttribute("href", i.MediaPath));

                itemNode.Add(new XAttribute("media-type", MimeTypeUtils.GetMime(i.ItemFile)));

                // 表紙画像のファイルの場合は cover-image プロパティをつける
                if (param.CoverImageFile != null && i.ItemFile.Name == param.CoverImageFile.Name)
                { itemNode.Add(new XAttribute("properties", "cover-image")); }

                // 目次ファイルの場合は nav プロパティをつける
                if (i.ID == "toc")
                { itemNode.Add(new XAttribute("properties", "nav")); }

                manifestNode.Add(itemNode);
            }

            logger.PrintPop();
            return manifestNode;
        }

        /// <summary>
        /// spine ノード (書籍上のページ情報) を生成する
        /// </summary>
        /// <param name="param">電子書籍のパラメータ</param>
        /// <param name="refs">電子書籍のページとして表示するオブジェクトの参照情報のリスト</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>spine ノード配下の xml 文字列</returns>
        private static XElement CreateSpineNode(
            CommonParams param,
            IEnumerable<ItemRefInfo> refs,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Generate spine node.");

            var spineNode = new XElement("spine");
            // ページの綴じ方向を示す属性を追加
            spineNode.Add(new XAttribute("page-progression-direction", (param.PageDirection == PageDirection.LeftToRight ? "ltr" : "rtl")));

            foreach (var r in refs)
            {
                var itemRefNode = new XElement("itemref");
                itemRefNode.Add(new XAttribute("linear", (r.IsLinear ? "yes" : "no")));
                itemRefNode.Add(new XAttribute("idref", r.RefID));
                if (r.PageSpreadMode != PageSpread.None)
                { itemRefNode.Add(new XAttribute("properties", r.PageSpreadMode.ToPageSpreadString())); }
                spineNode.Add(itemRefNode);
            }

            logger.PrintPop();
            return spineNode;
        }

        /// <summary>
        /// PageSpread を文字列に変換する
        /// </summary>
        /// <param name="ps"></param>
        /// <returns></returns>
        public static string ToPageSpreadString(this PageSpread ps)
        {
            switch (ps)
            {
                case PageSpread.SpreadLeft:
                    return "page-spread-left";
                case PageSpread.SPreadRight:
                    return "page-spread-right";
                case PageSpread.None:
                    return "none";
                default:
                    return null;
            }
        }
    }

}
