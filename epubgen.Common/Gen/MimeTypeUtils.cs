﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace epubgen.Gen
{
    /// <summary>
    /// 拡張子からMIMEを取得するためのユーティリティクラス
    /// </summary>
    public static class MimeTypeUtils
    {
        private static readonly Dictionary<string, string> Extentions = new Dictionary<string, string>()
        {
            { ".html", "application/xhtml+xml" },
            { ".xhtml", "application/xhtml+xml" },
            { ".css", "text/css" },
            { ".jpg", "image/jpeg" },
            { ".png", "image/png" },
            { ".gif", "image/gif" },
        };

        /// <summary>
        /// 指定したファイルの拡張子に応じた MIME 定義を取得する
        /// </summary>
        /// <param name="f">ファイル情報</param>
        /// <returns>指定した拡張子に対応したMIME定義. 拡張子に該当する定義が存在しない場合は null</returns>
        public static string GetMime(FileInfo f )
        {
            var ext = f.Extension.ToLower();
            return (Extentions.ContainsKey(ext) ? Extentions[ext] : null);
        }
    }
}
