﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using clUtils.log;
using clUtils.util;

using epubgen.Gen;
using epubgen.Plugin;
using epubgen.Types;

namespace epubgen
{
    /// <summary>
    /// 構築済みのプロジェクトディレクトリを圧縮してepubを作成するジェネレータ
    /// </summary>
    public class ProjectGenerator : IGenerator
    {
        /// <summary>
        /// usageを記述したリソースファイル名
        /// </summary>
        private static readonly string USAGE_RESOURCE_FILE_NAME = "epubgen.Resources.usage.txt";

        public string PluginType { get { return "project"; } }

        public Version VersionInfo { get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version; } }
        
        /// <summary>
        /// コマンドラインパラメータ
        /// </summary>
        private static CommandLineOptionDefinition[] ArgsTemplate =
        {
            // 圧縮対象のファイル群が格納されているフォルダ
            new CommandLineOptionDefinition(){ LongSwitch = "input-dir",  ShortSwitch = 'i', RequireParam = true, AllowMultiple = false },
        };

        public GenerateResult Generate(CommonParams cmmParams, CommandLineOptions options, ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Start generate epub");
            try
            {
                // 指定したディレクトリから書籍のGUIDを取得
                var inputDir = GetInputDirPath(options);
                cmmParams.epubID = EpubGeneratorUtil.GetEpubIDFromProject(inputDir, logger);
                if (Guid.Empty.Equals(cmmParams.epubID))
                {
                    logger.Print(LogLevel.ERROR, "ERROR: invaid opf file.");
                    return new GenerateResult()
                    {
                        Result = GenerateResultEnum.FAILED,
                        Exception = null,
                        OutputPath = null,
                    };
                }

                var epubFilePath = EpubGeneratorUtil.CreateEpubFilePath(cmmParams);
                return EpubGeneratorUtil.GenerateEpub(
                    inputDir,
                    epubFilePath,
                    cmmParams.AllowOverwrite,
                    logger);
            }
            catch (Exception eUnknown)
            {
                return new GenerateResult()
                {
                    Result = GenerateResultEnum.FAILED,
                    Exception = eUnknown,
                    OutputPath = null,
                };
            }
            finally
            { logger.PrintPop(); }
        }

        /// <summary>
        /// コマンドラインパラメータから入力ファイル群のディレクトリを取得して返す
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private DirectoryInfo GetInputDirPath(CommandLineOptions options)
        {
            if (options.Options.ContainsKey("input-dir"))
            {
                var inputDir = new DirectoryInfo(options.Options["input-dir"].ElementAt(0));
                if (!inputDir.Exists)
                { throw new DirectoryNotFoundException(string.Format("Input directory \"{0}\" not found.", inputDir.FullName)); }
                else
                { return inputDir; }
            }
            else
            { throw new InvalidOperationException("Input directory not found. require --input-dir switch."); }
        }

        public string Usage()
        {
            // usage を表示する
            var asm = Assembly.GetExecutingAssembly();
            using (var sr = new StreamReader(asm.GetManifestResourceStream(USAGE_RESOURCE_FILE_NAME)))
            {
                var text = sr.ReadToEnd();
                return text;
            }
        }
    }
}
