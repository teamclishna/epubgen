﻿Switches (project parameters):

Note:
  This plugin creates an epub file by compressing the directory specified by the input-dir switch.
  Ignore all common parameters except --output, --uuid, and --allow-overwirte.

  --input-dir <dir name>
    Specify the directory where the files for generating the epub file are stored.
	This switch cannot be omitted.
