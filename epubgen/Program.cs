﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using clUtils.log;
using clUtils.util;

using epubgen.Gen;
using epubgen.Plugin;
using epubgen.Types;

namespace epubgen
{
    /// <summary>
    /// アプリケーションのエントリポイント
    /// </summary>
    public class Program
    {
        /// <summary>
        /// usageを記述したリソースファイル名
        /// </summary>
        private static readonly string USAGE_RESOURCE_FILE_NAME = "epubgen.Resources.usage.txt";

        /// <summary>
        /// プログラムのエントリポイント
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        public static void Main(string[] args)
        { (new Program()).Start(args); }

        /// <summary>
        /// プログラムのエントリポイント.
        /// Mainメソッドからクラスのインスタンスを生成して呼び出される.
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        public void Start(string[] args)
        {
            // 使用可能なプラグインを走査する
            // プラグインが一つも存在しない場合はここで異常終了する
            var plugins = ScanPlugins(GetEntryPointDirectory());
            if (plugins.Count == 0)
            {
                Console.Error.WriteLine("ERROR: no plugin found.");
                return;
            }

            // コマンドラインパラメータ、またはjsonファイルから処理条件のパラメータを生成する.
            var cmdArgs = ParseCommandLineOptions(args);
            var commonParams = ArgsParseUtils
                .InitParameters()
                .SetParametersFromJson(cmdArgs)
                .SetParaemtersFromArgs(cmdArgs);

            // ロガーを生成する
            var output = CreateLogger(commonParams.Verbose);

            if(commonParams.ShowUsage)
            {
                Usage(plugins);
                return;
            }

            // epub 生成開始
            if(!plugins.ContainsKey(commonParams.BookType))
            {
                Console.Error.WriteLine("ERROR: not specify epub type. use --type switch.");
                return;
            }
            else
            {
                var result = plugins[commonParams.BookType].Generate(commonParams, cmdArgs, output);
                if(result.Result == GenerateResultEnum.SUCCESS)
                {
                    output.Print(
                        LogLevel.INFO,
                        string.Format(
                            "epub file: {0} generate succeeded.",
                            result.OutputPath));
                }
                else
                {
                    Console.Error.WriteLine("ERROR: epub generate failed.");
                    if (result.Exception != null)
                    { Console.Error.WriteLine(result.Exception.Message); }
                }
            }

            return;
        }

        /// <summary>
        /// このdllのあるディレクトリを返す
        /// </summary>
        /// <returns></returns>
        private DirectoryInfo GetEntryPointDirectory()
        { return new FileInfo(Assembly.GetExecutingAssembly().Location).Directory; }

        /// <summary>
        /// ロガーを生成する
        /// </summary>
        /// <param name="verbose">冗長な情報まで出力するならtrue</param>
        /// <returns>生成したロガー</returns>
        private ILogger CreateLogger(bool verbose)
        {
            // verbose オプションが指定されている場合は出力を詳細にする
            var output = LoggerFactory.CreateLogger(
                LoggerName.StdOutLogger,
                (verbose ?
                    new LogLevel[] { LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO, LogLevel.VERBOSE } :
                    new LogLevel[] { LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO })
                );
            output.ShowTimestamp = true;
            output.ShowThreadID = false;
            output.ShowCurrentLine = false;
            output.ShowCallStackIndentation = true;
            output.IndentString = "  ";
            return output;
        }

        /// <summary>
        /// 指定したディレクトリ内にあるdllからプラグイン情報を取得する.
        /// </summary>
        /// <param name="dir">スキャン元のディレクトリ</param>
        /// <returns>取得したプラグインオブジェクトのインスタンス</returns>
        private Dictionary<string, IGenerator> ScanPlugins(DirectoryInfo dir)
        {
            var result = new Dictionary<string, IGenerator>();

            foreach (var dll in dir.GetFiles("epubgen.*.dll"))
            {
                try
                {
                    // プラグインの型である IGenerator 型のインターフェイスを取得する
                    // ただし、IGenerator そのものは取得しない
                    var asm = Assembly.LoadFrom(dll.FullName);
                    foreach (var plugin in asm.GetTypes().Where((x) => { return (typeof(IGenerator).IsAssignableFrom(x) && (x != typeof(IGenerator))); }))
                    {
                        var i = Activator.CreateInstance(plugin) as IGenerator;
                        result.Add(i.PluginType, i);
                    }
                }
                catch(Exception)
                {
                    // プラグインの走査に失敗したdllファイルは無視する
                }
            }

            return result;
        }

        /// <summary>
        /// コマンドラインパラメータを解析して辞書に格納して返す
        /// </summary>
        /// <param name="args">Mainメソッドに渡されたコマンドライン引数</param>
        /// <returns>コマンドライン引数群を解析した辞書</returns>
        private CommandLineOptions ParseCommandLineOptions(string[] args)
        { return CommandLineUtils.Parse(args, ArgsParseUtils.COMMON_ARGS_TEMPLATE, true); }

        /// <summary>
        /// 作業用ディレクトリを生成するためのディレクトリを取得する
        /// </summary>
        /// <returns></returns>
        private DirectoryInfo CreateTempDir()
        {
            var dir = new DirectoryInfo(System.Environment.CurrentDirectory);
            return dir;
        }

        /// <summary>
        /// コマンドラインヘルプを表示する
        /// </summary>
        private void Usage(Dictionary<string, IGenerator> plugins)
        {
            // このアプリケーション本体のバージョン
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            var versionInfo = asm.GetName().Version;
            
            System.Console.Out.WriteLine(string.Format(
                "{0} {1}.{2}.{3}.{4}",
                asm.GetName().Name,
                versionInfo.Major,
                versionInfo.Minor,
                versionInfo.Build,
                versionInfo.Revision));

            // usage を表示する
            using (var sr = new StreamReader(asm.GetManifestResourceStream(USAGE_RESOURCE_FILE_NAME)))
            {
                var text = sr.ReadToEnd();
                System.Console.Out.Write(text);
            }

            // プラグインのバージョンとusageを表示する
            foreach (var k in plugins.Keys)
            {
                System.Console.Out.WriteLine("--------------------------------");

                var p = plugins[k];
                versionInfo = p.VersionInfo;
                System.Console.Out.WriteLine(string.Format(
                    "{0} {1}.{2}.{3}.{4}",
                    k,
                    versionInfo.Major,
                    versionInfo.Minor,
                    versionInfo.Build,
                    versionInfo.Revision));

                System.Console.Out.Write(p.Usage());
            }
        }
    }
}
