﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace epubgen.Types
{
    /// <summary>
    /// 処理内容を定義した構造体
    /// </summary>
    public class GenerateParam
    {
        /// <summary>
        /// この書籍のタイトル
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// この書籍の作者
        /// </summary>
        public string Author { get; set; }

        public Guid epubID { get; set; }

        /// <summary>
        /// epub ファイルを生成するためのファイル群を格納するディレクトリ
        /// </summary>
        public DirectoryInfo tempDir { get; set; }

        /// <summary>
        /// 漫画を指定した場合、使用する画像が格納されているディレクトリ
        /// </summary>
        public DirectoryInfo MangaInputDir { get; set; }

        /// <summary>
        /// 表紙画像に使用するファイル名
        /// </summary>
        public string CoverImageName { get; set; }

        /// <summary>
        /// epub ファイルに格納するファイルの情報を格納した辞書.
        /// キーとしてアイテムの ID を設定する
        /// </summary>
        public Dictionary<string, ItemInfo> Items { get; set; }

        /// <summary>
        /// epub内に格納するページ情報を格納したリスト.
        /// 書籍上のページ順に格納する.
        /// </summary>
        public List<ItemRefInfo> Refs { get; set; }
    }
}
