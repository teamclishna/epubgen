## epubファイルの構造

epubgen を実装するときに想定している epub ファイルの構成。
Sony Reader ではこれでいけたが他のビューアではだめ化もしれない。

* /
  * mimetype
  * navigation-documents.xhtml
  * <書籍名>.opf
  * META-INF/
    * container.xml
  * OEBPS/
    * images/
    * styles/
    * xhtml/

epubファイルの実態はzipファイル。
ただしアーカイブの先頭は必ず mimetype ファイルでなければならない。
Windows 標準の zip 圧縮機能では拡張情報が先頭に追加されるらしく、ビューアによっては正しく開けなくなる。

(Android の Sony Reader、bibi では開けた。EPUB-checker では警告が出た)

### /mimetype
以下の文字列だけを格納した20バイトのテキストファイル (改行も入れない)

```
application/epub+zip
```

### /navigation-documents.xhtml
目次の情報を格納したxmlファイル。

nav タグの中に ol タグのリストで目次項目を列挙する。
リンクのパスはルートを起点とする。

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html
 xmlns="http://www.w3.org/1999/xhtml"
 xmlns:epub="http://www.idpf.org/2007/ops"
 xml:lang="ja"
>
<head>
<meta charset="UTF-8"/>
<title>Navigation</title>
</head>
<body>
<nav epub:type="toc" id="toc">
  <h1>Navigation</h1>
  <ol>
    <li><a href="OEBPS/xhtml/p01.xhtml">第1章</a></li>
    <li><a href="OEBPS/xhtml/p02.xhtml">第2章</a></li>
    <li><a href="OEBPS/xhtml/p03.xhtml">第3章</a></li>
        :
        :
  </ol>
</nav>
</body>
</html>
```

### /META-INF/container.xml
epub ファイルの情報を設定する xml ファイル。
rootfile タグの full-path 属性にこの epub ファイルの書籍情報を格納したxmlファイルのパスを設定する。
media-type は固定値。
rootfile は rootfiles タグ内に格納しているが rootfile 情報を複数格納できるのかどうかは知らん。

```
<?xml version="1.0"?>
<container
  version="1.0"
  xmlns="urn:oasis:names:tc:opendocument:xmlns:container"
>
<rootfiles>
<rootfile
  full-path="mybook.opf"
  media-type="application/oebps-package+xml"
/>
</rootfiles>
</container>
```

### /<書籍名>.opf
書籍情報を設定する xml ファイル。
このアプリはこのファイルの内容を自動生成するのがメインの仕事になる。

```
<?xml version="1.0" encoding="UTF-8"?>
<package
  xmlns="http://www.idpf.org/2007/opf"
  version="3.0"
  xml:lang="ja"
  unique-identifier="unique-id"
  prefix="ebpaj: http://www.ebpaj.jp/"
>
<metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
	<dc:title id="title">書籍タイトル</dc:title>
	<meta refines="#title" property="file-as">ショセキタイトル</meta>
	<dc:creator id="creator01">著者名</dc:creator>
	<meta refines="#creator01" property="role" scheme="marc:relators">aut</meta>
	<meta refines="#creator01" property="file-as">チョシャメイ</meta>
	<meta refines="#creator01" property="display-seq">1</meta>

	<dc:language>ja</dc:language>
	<dc:identifier id="unique-id">urn:uuid:10e4df02-5ec5-45d7-9c44-000000000002</dc:identifier>
	<meta property="dcterms:modified">2020-01-02T00:00:00Z</meta>
	<meta property="ebpaj:guide-version">1.1.3</meta>
</metadata>
<manifest>
	<!-- cover image -->
	<item properties="cover-image" id="ci" href="OEBPS/images/cover.jpg" media-type="image/jpeg" />
	
	<!-- navigation -->
	<item media-type="application/xhtml+xml" id="toc" href="navigation-documents.xhtml" properties="nav"/>

	<!-- style -->
	<item media-type="text/css" id="fixed-layout-jp"   href="OEBPS/style/fixed-layout-jp.css"/>
	<item media-type="text/css" id="book-style"        href="OEBPS/style/book-style.css"/>
	<item media-type="text/css" id="style-reset"       href="OEBPS/style/style-reset.css"/>
	<item media-type="text/css" id="style-standard"    href="OEBPS/style/style-standard.css"/>
	<item media-type="text/css" id="style-advance"     href="OEBPS/style/style-advance.css"/>
	<item media-type="text/css" id="style-check"       href="OEBPS/style/style-check.css"/>

	<!-- xhtml -->
	<item media-type="application/xhtml+xml" id="p01"       href="OEBPS/xhtml/p01.xhtml"/>
	<item media-type="application/xhtml+xml" id="p02"       href="OEBPS/xhtml/p02.xhtml"/>
	<item media-type="application/xhtml+xml" id="p03"       href="OEBPS/xhtml/p03.xhtml"/>
        :
        :
</manifest>
<spine page-progression-direction="rtl">
	<itemref linear="yes" idref="p01" properties="page-spread-right"/>
	<itemref linear="yes" idref="p02" properties="page-spread-right"/>
	<itemref linear="yes" idref="p03" properties="page-spread-right"/>
        :
        :
</spine>
</package>
```

metadata/identifier タグには書籍ごとの GUID を設定する。
ビューアは多分ここで書籍を識別しているので、複数書籍を自作した時にここがダブってると片方消えたりする。
逆に修正した時にここを変えてしまうと同じ本が2冊になってしまう。

spine タグの page-progression-direction 属性で綴じ方向を設定する。
rtl にすると右綴じ (縦書き) の本になる。指定しなかった場合は ltr (左綴じ) になる。

spine/itemref タグで書籍内に実際に配置するページの情報を設定する。
properties 属性に page-spread-right / left 属性を指定すると、見開き時に左右どちらかに開始位置を強制できる。
このタグの idref には画像ファイルへの参照も直接指定できるが、その場合ページ見開きの指定が正しく動作しない。
漫画を自炊する場合でも画像を格納した html ファイルを作ること。

### /OEBPS フォルダ
書籍内のリソース (画像、スタイルシート、html ファイル) をここに配置する。
フォルダ名は多分必須ではない。フォルダ構造もおそらくこの通りでなくても良い。

このアプリでもフォルダ指定で漫画生成の場合は /OEBPS/resources フォルダに全リソースを入れている。
