﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using clUtils.log;
using clUtils.util;

using epubgen.Constants;
using epubgen.Gen;
using epubgen.Plugin;
using epubgen.Types;

namespace epubgen
{
    /// <summary>
    /// 単一のフォルダを指定してそのフォルダ内の画像をepub化するジェネレータ
    /// </summary>
    public class MangaGenerator : IGenerator
    {
        private static readonly string DIRNAME_METAINF = "META-INF";

        /// <summary>生成したリソースを配置するディレクトリのパス</summary>
        private static readonly string DIRNAME_RESOURCES = "OEBPS/resources";

        /// <summary>電子書籍のコンテナ情報を格納するxmlファイルのリソースパス</summary>
        private static readonly string TEMPLATE_XML_CONTAINER = "epubgen.Templates.container.xml";

        /// <summary>ページ画像を表示するためのxhtmlファイルのリソースパス</summary>
        private static readonly string TEMPLATE_HTML_MANGA = "epubgen.Templates.manga_page.xhtml";

        /// <summary>ページ画像を表示するためのスタイルシートのリソースパス</summary>
        private static readonly string TEMPLATE_CSS_MANGA = "epubgen.Templates.manga.css";

        /// <summary>ページ画像を表示するためのxhtmlファイルのリソースパス</summary>
        private static readonly string TEMPLATE_HTML_TOC = "epubgen.Templates.navigation-documents.xhtml";

        /// <summary>ページ画像のテンプレート中の画像パスに置換する箇所の文字列</summary>
        private static readonly string TEMPLATE_REPLACE_IMAGE_NAME = "%IMAGE_FILE_NAME%";

        /// <summary>usageを記述したリソースファイル名</summary>
        private static readonly string USAGE_RESOURCE_FILE_NAME = "epubgen.Resources.usage.txt";


        public string PluginType { get { return "manga"; } }

        public Version VersionInfo { get { return Assembly.GetExecutingAssembly().GetName().Version; } }

        /// <summary>
        /// コマンドラインパラメータ
        /// </summary>
        private static CommandLineOptionDefinition[] ArgsTemplate =
        {
            // 圧縮対象のファイル群が格納されているフォルダ
            new CommandLineOptionDefinition(){ LongSwitch = MangaArgNames.INPUT_DIR_LONG,  ShortSwitch = 'i', RequireParam = true, AllowMultiple = false },
            // 表紙画像のファイル名
            new CommandLineOptionDefinition(){ LongSwitch = MangaArgNames.COVER_IMAGE_LONG,  ShortSwitch = '\0', RequireParam = true, AllowMultiple = false },

            // ページの配置方向の指定
            // パラメータは <画像ファイル名>,<left | right> の形式で指定する
            // このスイッチは複数個指定可能
            new CommandLineOptionDefinition(){ LongSwitch = MangaArgNames.PAGE_SPREAD_LONG,  ShortSwitch = '\0', RequireParam = true, AllowMultiple = true },
        };

        public GenerateResult Generate(
            CommonParams cmmParams,
            CommandLineOptions options,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, "Start generate epub");
            try
            {
                // プラグイン固有のパラメータを取得
                var pluginParams = CommandLineUtils.Parse(options.Plain, ArgsTemplate, true);

                // 入力ディレクトリをから画像を取得する
                var inputDir = GetInputDirectory(pluginParams);
                var images = FindImageFiles(inputDir);

                // ページの見開き設定を取得
                var pageSpreadInfo = GetPageSpread(options, logger);

                // 作業ディレクトリを作成する
                // コマンドラインパラメータでUUID未設定の場合はここで生成する
                if (Guid.Empty.Equals(cmmParams.epubID))
                {
                    cmmParams.epubID = Guid.NewGuid();
                    logger.Print(LogLevel.INFO, string.Format("Generate new epub id: {0}.", cmmParams.epubID));
                }
                cmmParams.WorkingDirectory = CreateWorkDirectory(
                    cmmParams.WorkingBaseDirectory,
                    cmmParams.epubID.Guid2String(),
                    logger);

                var resourcesDir = cmmParams.WorkingDirectory
                    .GetDirectories("OEBPS")[0]
                    .GetDirectories("resources")[0];

                // 画像ファイルを作業用ディレクトリにコピーし、コピー後のファイル一覧を取得する
                // 取得した画像の1番目の要素を表紙画像にする
                var imageItems = CopyImageFiles(
                    images,
                    resourcesDir,
                    DIRNAME_RESOURCES,
                    logger);
                cmmParams.CoverImageFile = imageItems.ElementAt(0).ItemFile;

                // 画像を表示するためのxhtmlファイルを生成し、そのファイル情報を取得する
                var xhtmlItems = CreateXhtmlFiles(
                    imageItems,
                    resourcesDir,
                    DIRNAME_RESOURCES,
                    logger);

                var itemList = new List<ItemInfo>();

                // 目次情報を生成する
                // 目次はとりあえず1ページ目の xhtml を指定する
                var asm = Assembly.GetExecutingAssembly();
                using (var st = asm.GetManifestResourceStream(TEMPLATE_HTML_TOC))
                {
                    // リソース全体を読み込み、ファイルに書き出す
                    var tr = new StreamReader(st);
                    var tocTemplate = tr.ReadToEnd();

                    var tocItemInfo = NavigationDocumentsGenerator.CreateNavigationDocumentsFile(
                    cmmParams.WorkingDirectory,
                    cmmParams.Title,
                    tocTemplate,
                    new List<ItemInfo>() { xhtmlItems.ElementAt(0) },
                    logger);

                    itemList.Add(tocItemInfo);
                }

                itemList.AddRange(imageItems);
                itemList.AddRange(xhtmlItems);

                // スタイルシートを出力する
                CreateStyleSheet(resourcesDir, logger);

                // アイテム参照情報を生成する
                var itemRefs = CreateReferences(cmmParams, xhtmlItems, pageSpreadInfo, logger);

                // 画像のopfファイルを生成する
                var opfFileInfo = OpfGenerator.CreateOpfFile(
                    cmmParams.WorkingDirectory,
                    string.Format("{0}.opf", cmmParams.epubID.Guid2String()),
                    cmmParams,
                    itemList,
                    itemRefs,
                    logger);

                // container.xml ファイルを生成する
                using (var st = asm.GetManifestResourceStream(TEMPLATE_XML_CONTAINER))
                {
                    // リソース全体を読み込み、ファイルに書き出す
                    var tr = new StreamReader(st);
                    var containerTemplate = tr.ReadToEnd();
                    ContainerGenerator.CreateContainerFile(cmmParams.WorkingDirectory, containerTemplate, opfFileInfo.Name, logger);
                }

                // mimetypeファイルを生成する
                var mimetypeFileInfo = MimeTypeFileGenerator.CreateMimeTypeFile(
                    cmmParams.WorkingDirectory,
                    logger);

                // epubファイルの生成
                GenerateResult genResult = null;
                if (!cmmParams.DontGenerateEpub)
                {
                    genResult = EpubGeneratorUtil.GenerateEpub(
                        cmmParams.WorkingDirectory,
                        EpubGeneratorUtil.CreateEpubFilePath(cmmParams),
                        cmmParams.AllowOverwrite,
                        logger);
                }

                // 作業ディレクトリの削除
                // epub ファイルの生成に失敗した場合は削除しない
                if (!cmmParams.DontDeleteWorkDir)
                {
                    if (genResult == null || genResult.Result == GenerateResultEnum.SUCCESS)
                    {
                        logger.PrintPush(LogLevel.VERBOSE, string.Format("delete working directory: \"{0}\"", cmmParams.WorkingDirectory.FullName));
                        cmmParams.WorkingDirectory.Delete(true);
                        logger.PrintPop();
                    }
                }

                return new GenerateResult()
                {
                    Result = GenerateResultEnum.SUCCESS,
                    Exception = null,
                    OutputPath = null,
                };
            }
            catch (Exception eUnknown)
            {
                return new GenerateResult()
                {
                    Result = GenerateResultEnum.FAILED,
                    Exception = eUnknown,
                    OutputPath = null,
                };
            }
            finally
            { logger.PrintPop(); }
        }

        /// <summary>
        /// 入力画像の格納されているディレクトリを取得して返す
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private DirectoryInfo GetInputDirectory(CommandLineOptions args)
        {
            if (!args.Options.ContainsKey(MangaArgNames.INPUT_DIR_LONG))
            { throw new ArgumentException("require specify images directory with --input-dir switch."); }

            var inputDir = new DirectoryInfo(args.Options[MangaArgNames.INPUT_DIR_LONG].ElementAt(0));
            if (!inputDir.Exists)
            { throw new DirectoryNotFoundException(string.Format("images directory {0} not found.", inputDir.FullName)); }

            return inputDir;
        }

        /// <summary>
        /// 指定したディレクトリの直下にepubファイルの作業用ディレクトリを作成する
        /// </summary>
        /// <param name="baseDir"></param>
        /// <param name="dirName">作成するディレクトリ名</param>
        /// <param name="logger"></param>
        /// <returns>作成したディレクトリ</returns>
        private DirectoryInfo CreateWorkDirectory(DirectoryInfo baseDir, string dirName, ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, string.Format("Create working directory under \"{0}\"", baseDir.FullName));
            try
            {
                var subdir = baseDir.CreateSubdirectory(dirName);
                logger.Print(LogLevel.VERBOSE, string.Format("Create working directory \"{0}\"", subdir.FullName));

                var metaInfDir = subdir.CreateSubdirectory(DIRNAME_METAINF);
                logger.Print(LogLevel.VERBOSE, string.Format("Create working directory \"{0}\"", metaInfDir.FullName));

                var resourcesDir = subdir.CreateSubdirectory("OEBPS").CreateSubdirectory("resources");
                logger.Print(LogLevel.VERBOSE, string.Format("Create resources directory \"{0}\"", resourcesDir.FullName));

                return subdir;
            }
            catch (Exception eUnknown)
            {
                logger.Print(LogLevel.ERROR, string.Format("Create working directory failed: {0}", eUnknown.Message));
                logger.Print(LogLevel.ERROR, eUnknown.StackTrace);
                throw eUnknown;
            }
            finally
            {
                logger.PrintPop();
            }
        }

        /// <summary>
        /// コマンドラインオプションの page-spread スイッチから
        /// ページの見開き指定の情報を取得して返す.
        /// 本来はIDとしてリソースのIDを指定するが、ここでは代わりに指定したファイル名を格納する.
        /// </summary>
        /// <param name="options">コマンドラインオプション</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>
        /// 見開き設定を格納した辞書. ファイル名の拡張子を除いた文字列をキーとして見開きの情報を格納する.
        /// 1件も指定されていなかった場合は空の辞書を返す.
        /// </returns>
        private Dictionary<string, ItemRefInfo> GetPageSpread(CommandLineOptions options, ILogger logger)
        {
            logger.PrintPush(LogLevel.VERBOSE, "Get page spread info from --page-spread switches.");
            var result = new Dictionary<string, ItemRefInfo>();

            try
            {
                if (!options.Options.ContainsKey(MangaArgNames.PAGE_SPREAD_LONG))
                { return result; }

                var pageSpreadSwitches = options.Options[MangaArgNames.PAGE_SPREAD_LONG];

                // パラメータが指定されていなかった場合
                if (pageSpreadSwitches.Count() == 0)
                {
                    logger.Print(LogLevel.WARN, "No parameter in page-spread switch. ignore this.");
                    return result;
                }

                foreach (var ps in pageSpreadSwitches)
                {
                    // パラメータが指定されていなかった場合は警告を表示して無視する
                    if (ps == null)
                    {
                        logger.Print(LogLevel.WARN, "No parameter in page-spread switch. ignore this.");
                        continue;
                    }

                    // パラメータはファイル名と見開き情報がカンマ区切りで繋がれている想定である.
                    // 想定と異なる書式の場合は警告表示して無視、次へ進む
                    var psParams = ps.Split(',');
                    if (psParams.Length != 2)
                    {
                        logger.Print(LogLevel.WARN, string.Format("Invalid parameter \"{0}\"in page-spread switch. ignore this.", ps));
                        continue;
                    }

                    var imageFileInfo = new FileInfo(psParams[0].Trim());
                    var fileName = imageFileInfo.Name.Replace(imageFileInfo.Extension, "");
                    var align = psParams[1].Trim().ToLower();
                    switch (align)
                    {
                        case "left":
                            result.Add(fileName, new ItemRefInfo() { RefID = fileName, IsLinear = true, PageSpreadMode = PageSpread.SpreadLeft });
                            break;

                        case "right":
                            result.Add(fileName, new ItemRefInfo() { RefID = fileName, IsLinear = true, PageSpreadMode = PageSpread.SPreadRight });
                            break;

                        default:
                            logger.Print(LogLevel.WARN, string.Format("Invalid parameter \"{0}\"in page-spread switch. ignore this.", ps));
                            break;
                    }
                }
                return result;
            }
            finally
            { logger.PrintPop(); }
        }

        /// <summary>
        /// 指定したディレクトリにある画像ファイル名を走査してリストに格納して返す.
        /// </summary>
        /// <param name="scanDir">走査対象のディレクトリ. サブディレクトリは検索しない</param>
        /// <returns>ディレクトリにある画像ファイルのリスト (拡張子 .jpg, .gif, .png) ファイル名順にソートして返す</returns>
        private IEnumerable<FileInfo> FindImageFiles(DirectoryInfo scanDir)
        {
            string[] exts = new string[] { ".jpg", ".png", ".png", };
            return scanDir
                .GetFiles()
                .Where(x => exts.Contains(x.Extension.ToLower()))
                .OrderBy(x => x.Name);
        }

        /// <summary>
        /// 前もって取得した入力画像ファイルを作業用ディレクトリにコピーする
        /// </summary>
        /// <param name="files">コピー対象の画像ファイルの情報</param>
        /// <param name="destDir">コピー先のディレクトリ</param>
        /// <returns>コピー後のファイルのアイテム情報. この時、ファイル名順に "i000" で書式化した ID を割り当てる</returns>
        private IEnumerable<ItemInfo> CopyImageFiles(
            IEnumerable<FileInfo> files,
            DirectoryInfo destDir,
            string relativeDir,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.VERBOSE, string.Format("Copy Images to {0}.", destDir.FullName));
            var result = new List<ItemInfo>();
            var destDirPath = destDir.FullName;

            foreach (var f in files)
            {
                logger.Print(LogLevel.VERBOSE, string.Format("{0} -> {1}", f.Name, destDirPath));
                var copiedFile = f.CopyTo(Path.Combine(destDirPath, f.Name), true);

                result.Add(new ItemInfo()
                {
                    ID = string.Format("img{0:000}", (result.Count + 1)),
                    ItemFile = copiedFile,
                    MediaPath = relativeDir + "/" + copiedFile.Name,
                    MediaType = ""
                });
            }
            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 指定した画像ファイルを表示するためのxhtmlファイルを生成し、そのファイルのアイテム情報を生成して返す
        /// </summary>
        /// <param name="imageFiles">表示対象の画像ファイル</param>
        /// <param name="destDir">画像を配置するディレクトリ</param>
        /// <param name="relativeDir">destDir で指定したディレクトリの相対パス</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>xhtmlファイルのアイテム情報. この時、ファイル名順に "h000" で書式化した ID を割り当てる</returns>
        private IEnumerable<ItemInfo> CreateXhtmlFiles(
            IEnumerable<ItemInfo> imageFiles,
            DirectoryInfo destDir,
            string relativeDir,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.VERBOSE, "Create xhtml files.");
            var result = new List<ItemInfo>();

            // ページ画像を表示するためのxhtmlのテンプレートを取得する
            var asm = Assembly.GetExecutingAssembly();
            using (var st = asm.GetManifestResourceStream(TEMPLATE_HTML_MANGA))
            {
                // テンプレートを読み込み、置換部分をファイル名、作品タイトルに入れ替える
                var tr = new StreamReader(st);
                var xhtmlTemplate = tr.ReadToEnd();

                int index = 1;
                foreach (var img in imageFiles)
                {
                    result.Add(CreateXHTMLFile(
                        img,
                        string.Format("h{0:000}", index),
                        xhtmlTemplate,
                        logger));

                    index++;
                }
            }
            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 指定した画像を格納するためのxhtmlファイルを生成し、そのアイテムを格納するための情報を返す
        /// </summary>
        /// <param name="src">格納対象の画像ファイルの情報</param>
        /// <param name="itemID">生成するhtmlに割り当てるアイテムID</param>
        /// <param name="templateString">生成するxhtmlファイルのテンプレート</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>この画像ファイルに関するxhtmlのアイテム情報</returns>
        private ItemInfo CreateXHTMLFile(
            ItemInfo src,
            string itemID,
            string templateString,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.INFO, string.Format("Generate xhtml file for {0}", src.ItemFile.Name));

            // テンプレートの文字列を画像ファイルのパスで置換する
            // 画像と同じディレクトリにxhtmlを出力するので相対パスは不要.
            var replacedXHTML = templateString.Replace(TEMPLATE_REPLACE_IMAGE_NAME, src.ItemFile.Name);
            var writeFileInfo = new FileInfo(Path.Combine(
                src.ItemFile.Directory.FullName,
                src.ItemFile.Name.Replace(src.ItemFile.Extension, ".xhtml")));

            using (var sw = new StreamWriter(
                new FileStream(writeFileInfo.FullName, FileMode.CreateNew),
                new UTF8Encoding(false)))
            { sw.Write(replacedXHTML); }

            // アイテム情報を生成
            logger.Print(LogLevel.VERBOSE, string.Format("xhtml file \"{0}\" generated as \"{1}\".", writeFileInfo.FullName, itemID));
            var result = new ItemInfo()
            {
                ID = itemID,
                ItemFile = writeFileInfo,
                MediaPath = DIRNAME_RESOURCES + "/" + writeFileInfo.Name,
                MediaType = MimeTypeUtils.GetMime(writeFileInfo),
            };

            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// スタイルシートをリソースから取り出し、ディレクトリに配置する
        /// </summary>
        /// <param name="destDir">スタイルシートを配置するディレクトリ</param>
        /// <param name="logger">ロガーオブジェクト</param>
        private void CreateStyleSheet(DirectoryInfo destDir, ILogger logger)
        {
            // ページ画像を表示するためのxhtmlのテンプレートを取得する
            var asm = Assembly.GetExecutingAssembly();
            using (var st = asm.GetManifestResourceStream(TEMPLATE_CSS_MANGA))
            {
                // リソース全体を読み込み、ファイルに書き出す
                var tr = new StreamReader(st);
                var cssTemplate = tr.ReadToEnd();
                var writePath = Path.Combine(destDir.FullName, "manga.css");

                using (var sw = new StreamWriter(
                    new FileStream(writePath, FileMode.CreateNew),
                    new UTF8Encoding(false)))
                { sw.Write(cssTemplate); }

                logger.Print(LogLevel.VERBOSE, string.Format("Write css file \"{0}\".", writePath));
            }
        }

        /// <summary>
        /// 書籍のページ情報を定義するためのitemref情報を生成する
        /// </summary>
        /// <param name="commParams">共通パラメータ</param>
        /// <param name="xhtmlFiles">各ページの情報を格納したxhtmlのアイテム情報</param>
        /// <param name="pageSpreadInfo">ページの見開き情報</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns></returns>
        private IEnumerable<ItemRefInfo> CreateReferences(
            CommonParams commParams,
            IEnumerable<ItemInfo> xhtmlFiles,
            Dictionary<string, ItemRefInfo> pageSpreadInfo,
            ILogger logger)
        {
            logger.PrintPush(LogLevel.VERBOSE, "Generate itemRef");
            var result = new List<ItemRefInfo>();

            foreach (var i in xhtmlFiles)
            {
                ItemRefInfo refInfo;

                if (result.Count == 0)
                {
                    // 最初の1ページ (表1) は綴じ方向に合わせて設定する
                    refInfo = new ItemRefInfo()
                    {
                        RefID = i.ID,
                        IsLinear = true,
                        PageSpreadMode = (commParams.PageDirection == PageDirection.LeftToRight ? PageSpread.SPreadRight : PageSpread.SpreadLeft),
                    };
                }
                else if (result.Count == 1)
                {
                    // 2ページ目 (本文最初のページ) は明示的な指定が無い限りは表1と同じ方向にする
                    var keyName = i.ItemFile.Name.Replace(i.ItemFile.Extension, "");
                    refInfo = new ItemRefInfo()
                    {
                        RefID = i.ID,
                        IsLinear = true,
                        PageSpreadMode = (pageSpreadInfo.ContainsKey(keyName) ? pageSpreadInfo[keyName] : result[0]).PageSpreadMode,
                    };
                }
                else
                {
                    // 事前に画像ファイル名で指定した見開き情報が存在すればそれに従ってページ方向を設定する
                    // pageSpreadInfo には画像ファイル名をキーとして格納
                    var ps = PageSpread.None;
                    var keyName = i.ItemFile.Name.Replace(i.ItemFile.Extension, "");
                    if (pageSpreadInfo.ContainsKey(keyName))
                    { ps = pageSpreadInfo[keyName].PageSpreadMode; }

                    refInfo = new ItemRefInfo()
                    {
                        RefID = i.ID,
                        IsLinear = true,
                        PageSpreadMode = ps,
                    };
                }

                logger.Print(
                    LogLevel.VERBOSE,
                    string.Format(
                        "Add itemref ID:\"{0}\" PageSpread:\"{1}\".",
                        refInfo.RefID,
                        refInfo.PageSpreadMode.ToString()));
                result.Add(refInfo);
            }

            logger.PrintPop();
            return result;
        }

        public string Usage()
        {
            // usage を表示する
            var asm = Assembly.GetExecutingAssembly();
            using (var sr = new StreamReader(asm.GetManifestResourceStream(USAGE_RESOURCE_FILE_NAME)))
            {
                var text = sr.ReadToEnd();
                return text;
            }
        }
    }
}
