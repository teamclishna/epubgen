﻿using System;
using System.Collections.Generic;
using System.Text;

namespace epubgen.Constants
{
    /// <summary>
    /// manga のコマンドライン引数パラメータ名
    /// </summary>
    public static class MangaArgNames
    {
        /// <summary>入力画像のフォルダパス</summary>
        public static readonly string INPUT_DIR_LONG = "input-dir";
        /// <summary>表紙画像のファイル名</summary>
        public static readonly string COVER_IMAGE_LONG = "cover-image";
        /// <summary>ページの配置方向</summary>
        public static readonly string PAGE_SPREAD_LONG = "page-spread";
    }
}
